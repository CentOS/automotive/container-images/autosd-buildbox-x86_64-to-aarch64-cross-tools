FROM quay.io/centos-sig-automotive/autosd-buildbox:latest

LABEL com.github.containers.toolbox="true" \
      com.redhat.component="$NAME" \
      name="$NAME" \
      version="$VERSION" \
      usage="This image can be used with the toolbox command" \
      summary="Base image for building AutoSD packages with cross compilation tools"

# copy configuration files to the filesystem
COPY rpms.in /rpms.in

# Install utility tools then uninstall epel-release to ensure we don't install
# the cross-compiler from EPEL
RUN dnf install -y centpkg koji

# Install the cross-arch packages
RUN koji -p stream download-build -a x86_64 --latestfrom=c9s-gate gcc && \
    koji -p stream download-build -a x86_64 --latestfrom=c9s-gate binutils && \
    dnf install --disablerepo=epel -y ./cross-binutils-aarch64*.rpm \
        ./cross-gcc-aarch64*.rpm \
        ./cross-gcc-c++-aarch64*.rpm

RUN mkdir -pv /usr/aarch64-redhat-linux/sys-root/el9 && \
    dnf --forcearch=aarch64 \
    --installroot=/usr/aarch64-redhat-linux/sys-root/el9 \
    --nogpgcheck \
    --releasever=9 install -y \
    glibc-devel libstdc++-devel libzstd-devel \
    binutils-devel e2fsprogs-devel fuse-devel \
    gcc-plugin-devel glib2-devel libcap-devel \
    libcap-ng-devel libmnl-devel numactl-devel \
    openssl-devel perl-devel python3-devel gpgme-devel

# install additional packages for kernel builds
RUN dnf -y --skip-broken --best --allowerasing install $(cat /rpms.in) && \
    dnf clean all
